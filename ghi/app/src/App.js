import { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import  {createBrowserRouter, RouterProvider, BrowserRouter, Routes, Route} from "react-router-dom"
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeesForm from './AttendeesForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  const [ attendees, setAttendees ] = useState([]);

  async function getAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees');
    if (response.ok) {
      const { attendees } = await response.json();
      setAttendees(attendees);
    } else {
      console.error('An error has occurred fetching the data')
    }
  }

    useEffect(() => {
      getAttendees();
    }, [])

  if (attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendeesForm refreshAttendees={getAttendees} />} />
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={attendees} />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
