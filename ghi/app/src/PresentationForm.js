import React, {useEffect, useState} from 'react';

function PresentationForm(props) {
  const [presenterName, setPresenterName] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [created, setCreated] = useState('');
  const [conference, setConference] = useState('');
  const [conferences, setConferences] = useState([]);

  const handlePresenterNameChange = (event) => {
    const value = event.target.value;
    setPresenterName(value);
  }

  const handleCompanyNameChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }

  const handlePresenterEmailChange = (event) => {
    const value = event.target.value;
    setPresenterEmail(value);
  }

  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  }

  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  }

  const handleCreatedChange = (event) => {
    const value = event.target.value;
    setCreated(value);
  }

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.presenter_name = presenterName;
    data.company_name = companyName;
    data.presenter_email = presenterEmail;
    data.title = title;
    data.synopsis = synopsis;
    data.created = created;
    data.conference = conference;


    const presentationUrl = `http://localhost:8000${conference}presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      setPresenterName('');
      setCompanyName('');
      setPresenterEmail('');
      setTitle('');
      setSynopsis('');
      setCreated('');
      setConference('');
      setConferences([]);
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange} placeholder="Presenter Name" required type="text" id="presenter_name" className="form-control" value={presenterName}/>
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company Name" required type="text" id="company_name" className="form-control" value={companyName} />
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange} placeholder="Presenter Email" required type="email" id="presenter_email" className="form-control" value={presenterEmail} />
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" required type="text" id="title" className="form-control" value={title} />
                <label htmlFor="title">Title</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleSynopsisChange} placeholder="Synopsis" required type="text" id="synopsis" className="form-control" value={synopsis}></textarea>
                <label htmlFor="synopsis">Synopsis</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCreatedChange} placeholder="Created" required type="date" id="created" className="form-control" value={created} />
                <label htmlFor="created">Created</label>
              </div>
              <div className="mb-3">
                  <select onChange={handleConferenceChange} name="conference" id="conference" className="form-select" required>
                    <option value="">Choose a Conference</option>
                    {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })}
                  </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default PresentationForm;
