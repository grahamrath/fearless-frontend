import React, {useEffect, useState} from 'react';

function ConferenceForm (props) {
  const [conferenceName, setConferenceName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maximumPresentation, setMaximumPresentation] = useState('');
  const [maximumAttendees, setMaximumAttendees] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  const handleConferenceNameChange = (event) => {
    const value = event.target.value;
    setConferenceName(value);
  }

  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaximumPresentationChange = (event) => {
    const value = event.target.value;
    setMaximumPresentation(value);
  }

  const handleMaximumAttendeesChange = (event) => {
    const value = event.target.value;
    setMaximumAttendees(value);
  }

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = conferenceName;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maximumPresentation;
    data.max_attendees = maximumAttendees;
    data.location = location;

    const locationUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      setConferenceName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaximumPresentation('');
      setMaximumAttendees('');
      setLocation('');
      setLocations([]);
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations (data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Conference</h1>
              <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange={handleConferenceNameChange} placeholder="Name" required type="text" id="name" className="form-control" value={conferenceName} />
                  <label htmlFor="name">Conference Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStartsChange} placeholder="Start Date" required type="date" id="start_date" className="form-control" value={starts} />
                  <label htmlFor="start_date">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEndsChange}placeholder="End Date" required type="date" id="end_date" className="form-control" value={ends} />
                  <label htmlFor="end_date">Ends</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleDescriptionChange} placeholder="Description" required type="text" id="description" className="form-control" value={description}></textarea>
                  <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleMaximumPresentationChange} placeholder="Maximum Presentations" required type="number" id="maximum_presentations" className="form-control" value={maximumPresentation} />
                  <label htmlFor="maximum_presentations">Maximum Presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleMaximumAttendeesChange} placeholder="Maximum Attendees" required type="number" id="maximum_attendees" className="form-control" value={maximumAttendees} />
                  <label htmlFor="maximum_attendees">Maximum Attendees</label>
                </div>
                <div className="mb-3">
                <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.name} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
